#!/usr/bin/env python

import sys

def drepr(x, sort = True, indent = 0, shift = 2):
  if isinstance(x, dict):
    r = '{\n'
    for (key, value) in (sorted(x.items()) if sort else x.iteritems()):
      r += (' ' * (indent + shift)) + repr(key) + ': '
      r += drepr(value, sort, indent + shift) + ',\n'
    r = r.rstrip(',\n') + '\n'
    r += (' ' * indent) + '}'
  elif hasattr(x, '__iter__'):
    r = '[\n'
    for value in (sorted(x) if sort else x):
      r += (' ' * (indent + shift)) + drepr(value, sort, indent + shift) + ',\n'
    r = r.rstrip(',\n') + '\n'
    r += (' ' * indent) + ']'
  else:
    r = repr(x)
  return r


files = sys.argv[1:]

result = {}

for f in files:
  try:
    tmp = eval(open(f, 'r').read().strip())
    for k in tmp.iterkeys():
      if result.has_key(k):
        print "Run", k, "has been already found in previous files! Skipping ..."
        del tmp[k]
    result.update(tmp)
  except:
    print "Error with file", f

print drepr(result)
