#!/bin/bash

# The script prints to stdout JSON data for given runs which contains continuous intervals of lumisection in which ECAL is GREEN and beam is Stable.
# Script requires valide Kerberos ticket and should be run from lxplus.cern.ch (or other node with cern-get-sso-cookie package installed)
#
# Author: Latyshev Grigory
# Date: 07/07/2015

tmpdir=$(mktemp -d)

cern-get-sso-cookie --krb -r -u "https://cmswbm.web.cern.ch" -o $tmpdir/ssocookie.txt

chmod 600 $tmpdir/ssocookie.txt

runs="$@"
if [ -z "$runs" -o "$1" == "-h" -o "$1" == "--help" ];then
  echo "Usage: bash {0} <run> <run> <run> ... <run>"
  exit 0
fi

filelist=""

for r in $(echo $runs | tr ' ' '\n'); do 
  curl -s -L --cookie $tmpdir/ssocookie.txt --cookie-jar $tmpdir/ssocookie.txt https://cmswbm.web.cern.ch/cmswbm/cmsdb/servlet/LumiSections?RUN=$r > $tmpdir/$r
  [ $? -eq 0 ] && filelist="$filelist $tmpdir/$r"
done

# python analyser
cat <<EOF > $tmpdir/analyser.py
#!/usr/bin/env python

import sys
import os
import re
from operator import itemgetter
from itertools import groupby

lumire = re.compile("<TD.*LUMISECTION=(.+)>.+</A>")
condre = lambda x: re.compile(".*TITLE=\"{0}\" BGCOLOR=GREEN.*".format(x))

def getLumisList(filename):
  f = open(filename, 'r')
  lumis = []
  for l in f.readlines():
    l = l.strip()
    if lumire.match(l):
      lumi = int(lumire.findall(l)[0])
      continue
    if  condre("beam1Present").match(l) and condre("beam2Present").match(l) \
    and condre("beam1Stable").match(l) and condre("beam2Stable").match(l) \
    and condre("ebpReady").match(l) and condre("ebmReady").match(l) \
    and condre("eepReady").match(l) and condre("eemReady").match(l) \
    and condre("Run Active").match(l):
      lumis.append(lumi)
    else:
      continue
  f.close()
  return lumis

def split2Intervals(l):
  ranges = []
  for k, g in groupby(enumerate(l), lambda (i,x):i-x):
      group = map(itemgetter(1), g)
      ranges.append([group[0], group[-1]])
  return ranges

def drepr(x, sort = True, indent = 0, shift = 2):
  if isinstance(x, dict):
    r = '{\n'
    for (key, value) in (sorted(x.items()) if sort else x.iteritems()):
      r += (' ' * (indent + shift)) + repr(key) + ': '
      r += drepr(value, sort, indent + shift) + ',\n'
    r = r.rstrip(',\n') + '\n'
    r += (' ' * indent) + '}'
  elif hasattr(x, '__iter__'):
    r = '[\n'
    for value in (sorted(x) if sort else x):
      r += (' ' * (indent + shift)) + drepr(value, sort, indent + shift) + ',\n'
    r = r.rstrip(',\n') + '\n'
    r += (' ' * indent) + ']'
  else:
    r = repr(x)
  return r

runs = sys.argv[1:]
info = {}

for r in runs:
  info.update({os.path.basename(str(r)) : split2Intervals(getLumisList(r))})

print drepr(info, shift = 2)
EOF

python $tmpdir/analyser.py $filelist
